package com.vecturagames.bigfilesfinder.model;

import java.io.File;
import java.util.ArrayList;

public interface IFindBigFilesResult {

    boolean tryAddFile(File file);
    ArrayList<File> getFiles();
    int getSearchedFilesCount();
    void incSearchedFilesCount();

}
