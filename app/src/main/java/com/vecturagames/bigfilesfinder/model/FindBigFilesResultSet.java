package com.vecturagames.bigfilesfinder.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

public class FindBigFilesResultSet implements IFindBigFilesResult {

    private HashSet<File> mFiles = new HashSet<File>();
    private int mBigFilesCount = 0;

    private File mSmallestBigFile = null;
    private long mSmallestBigFileSize = Long.MAX_VALUE;

    private int mSearchedFilesCount = 0;

    public FindBigFilesResultSet(int bigFilesCount) {
        mBigFilesCount = bigFilesCount;
    }

    @Override
    public synchronized boolean tryAddFile(File file) {
        mSearchedFilesCount++;

        long fileSize = file.length();

        if ((mSmallestBigFile != null && fileSize > mSmallestBigFileSize) || mFiles.size() < mBigFilesCount) {
            if (mFiles.size() == mBigFilesCount) {
                mFiles.remove(mSmallestBigFile);
            }

            mFiles.add(file);

            mSmallestBigFile = null;
            mSmallestBigFileSize = Long.MAX_VALUE;
            for (File f : mFiles) {
                if (mSmallestBigFile == null || f.length() < mSmallestBigFileSize) {
                    mSmallestBigFile = f;
                    mSmallestBigFileSize = mSmallestBigFile.length();
                }
            }

            return true;
        }

        return false;
    }

    @Override
    public ArrayList<File> getFiles() {
        Comparator<File> sizeComparator = new Comparator<File>() {
            @Override
            public int compare(File first, File second) {
                return ((Long)second.length()).compareTo(first.length());
            }
        };

        ArrayList<File> result = new ArrayList<File>();
        for (File file : mFiles) {
            result.add(file);
        }
        Collections.sort(result, sizeComparator);

        return result;
    }

    @Override
    public int getSearchedFilesCount() {
        return mSearchedFilesCount;
    }

    @Override
    public void incSearchedFilesCount() {
        mSearchedFilesCount++;
    }

}
