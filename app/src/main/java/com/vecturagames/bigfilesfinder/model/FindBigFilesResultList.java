package com.vecturagames.bigfilesfinder.model;

import java.io.File;
import java.util.ArrayList;

public class FindBigFilesResultList implements IFindBigFilesResult {

    private ArrayList<File> mFiles = new ArrayList<File>();
    private int mBigFilesCount = 0;

    private File mSmallestBigFile = null;
    private long mSmallestBigFileSize = Long.MAX_VALUE;

    private int mSearchedFilesCount = 0;

    public FindBigFilesResultList(int bigFilesCount) {
        mBigFilesCount = bigFilesCount;
    }

    @Override
    public synchronized boolean tryAddFile(File file) {
        mSearchedFilesCount++;

        long fileSize = file.length();

        if ((mSmallestBigFile != null && fileSize > mSmallestBigFileSize) || mFiles.size() < mBigFilesCount) {
            boolean added = false;

            if (mFiles.size() == mBigFilesCount) {
                mFiles.remove(mFiles.size() - 1);
            }

            for (int i = mFiles.size() - 1; i >= 0; i--) {
                if (mFiles.get(i).length() > fileSize) {
                    mFiles.add(i + 1, file);

                    added = true;
                    break;
                }
            }

            if (!added) {
                mFiles.add(0, file);
            }

            mSmallestBigFile = mFiles.get(mFiles.size() - 1);
            mSmallestBigFileSize = mSmallestBigFile.length();

            return true;
        }

        return false;
    }

    @Override
    public ArrayList<File> getFiles() {
        return mFiles;
    }

    @Override
    public int getSearchedFilesCount() {
        return mSearchedFilesCount;
    }

    @Override
    public void incSearchedFilesCount() {
        mSearchedFilesCount++;
    }

}
