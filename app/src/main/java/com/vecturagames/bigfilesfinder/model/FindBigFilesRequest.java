package com.vecturagames.bigfilesfinder.model;

import com.vecturagames.bigfilesfinder.util.Util;

import java.io.File;
import java.util.HashSet;

public class FindBigFilesRequest {

    private HashSet<File> mChosenDirectories;
    private int mBigFilesCount;

    public FindBigFilesRequest() {
        mChosenDirectories = new HashSet<>();
        mBigFilesCount = 0;
    }

    public FindBigFilesRequest(FindBigFilesRequest findBigFilesRequest) {
        mChosenDirectories = new HashSet<File>(findBigFilesRequest.mChosenDirectories);
        mBigFilesCount = findBigFilesRequest.getBigFilesCount();
    }

    public boolean chosenDirectoriesContainsParent(File dir) {
        String dirPath = dir.getAbsolutePath();

        for (File chosenDir : mChosenDirectories) {
            String chosenDirPath = Util.addTrailingFileSeparatorIfNotPresent(chosenDir.getAbsolutePath());

            if (dirPath.startsWith(chosenDirPath) && !dir.equals(chosenDir)) {
                return true;
            }
        }

        return false;
    }

    public boolean chosenDirectoriesContains(File dir) {
        return mChosenDirectories.contains(dir);
    }

    public HashSet<File> getChosenDirectories() {
        return mChosenDirectories;
    }

    public int getBigFilesCount() {
        return mBigFilesCount;
    }

    public void setBigFilesCount(int bigFilesCount) {
        mBigFilesCount = bigFilesCount;
    }

}
