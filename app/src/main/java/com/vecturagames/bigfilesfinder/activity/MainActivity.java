package com.vecturagames.bigfilesfinder.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Environment;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.vecturagames.bigfilesfinder.R;
import com.vecturagames.bigfilesfinder.callback.DirectoryChooserAdapterCallbacks;
import com.vecturagames.bigfilesfinder.callback.FindBigFilesServiceCallbacks;
import com.vecturagames.bigfilesfinder.fragment.DirectoryChooserFragment;
import com.vecturagames.bigfilesfinder.fragment.FindResultsFragment;
import com.vecturagames.bigfilesfinder.model.FindBigFilesRequest;
import com.vecturagames.bigfilesfinder.model.IFindBigFilesResult;
import com.vecturagames.bigfilesfinder.service.FindBigFilesService;

import java.io.File;
import java.lang.reflect.Type;

import static com.vecturagames.bigfilesfinder.R.id.tabLayout;

public class MainActivity extends AppCompatActivity implements DirectoryChooserAdapterCallbacks, FindBigFilesServiceCallbacks {

    public static final String BROADCAST_ACTION_OPEN_DIRECTORY = "BROADCAST_ACTION_OPEN_DIRECTORY";
    public static final String BROADCAST_ACTION_SHOW_RESULTS = "BROADCAST_ACTION_SHOW_RESULTS";
    public static final String BROADCAST_ARG_OPEN_DIRECTORY_FILE = "BROADCAST_ARG_OPEN_DIRECTORY_FILE";
    public static final String BROADCAST_ARG_SHOW_RESULTS_SET_OF_RESULTS = "BROADCAST_ARG_SHOW_RESULTS_SET_OF_RESULTS";

    private static final String ARG_FIND_BIG_FILES_REQUEST = "ARG_FIND_BIG_FILES_REQUEST";

    private static final int TAB_IDX_DIRECTORY_CHOOSER = 0;
    private static final int TAB_IDX_FIND_RESULTS = 1;

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private CustomPagerAdapter mPagerAdapter;
    private Button mButtonStopFinding;
    private Button mButtonStartFinding;

    private CustomServiceConnection mServiceConnection = new CustomServiceConnection();
    private FindBigFilesService mService;
    private boolean mBound = false;

    private FindBigFilesRequest mFindBigFilesRequest = new FindBigFilesRequest();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            if (savedInstanceState.containsKey(ARG_FIND_BIG_FILES_REQUEST)) {
                String jsonString = savedInstanceState.getString(ARG_FIND_BIG_FILES_REQUEST);

                if (jsonString != null && !jsonString.equals("")) {
                    Type type = new TypeToken<FindBigFilesRequest>() { }.getType();
                    try {
                        mFindBigFilesRequest = gson.fromJson(jsonString, type);
                    }
                    catch (JsonSyntaxException e) {
                        Log.v("LogBigFilesFinder", "MainActivity.onCreate(): exception occurred while getting FindBigFilesRequest from JSON string: " + e.getMessage());
                    }
                }
            }
        }

        initActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mBound) {
            unbindService(mServiceConnection);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        outState.putString(ARG_FIND_BIG_FILES_REQUEST, gson.toJson(mFindBigFilesRequest));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_root_directory: {
                TabLayout.Tab tab = mTabLayout.getTabAt(TAB_IDX_DIRECTORY_CHOOSER);

                if (tab != null) {
                    tab.select();

                    Intent intent = new Intent(BROADCAST_ACTION_OPEN_DIRECTORY);
                    intent.putExtra(BROADCAST_ARG_OPEN_DIRECTORY_FILE, Environment.getRootDirectory());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                }
                return true;
            }

            case R.id.menu_external_storage_directory: {
                TabLayout.Tab tab = mTabLayout.getTabAt(TAB_IDX_DIRECTORY_CHOOSER);

                if (tab != null) {
                    tab.select();

                    Intent intent = new Intent(BROADCAST_ACTION_OPEN_DIRECTORY);
                    intent.putExtra(BROADCAST_ARG_OPEN_DIRECTORY_FILE, Environment.getExternalStorageDirectory());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                }
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCannotOpenDirectoryListener(File dir) {

    }

    @Override
    public void onDirectoryClickListener(File dir) {

    }

    @Override
    public boolean canSelectDirectory(File dir) {
        return mFindBigFilesRequest.chosenDirectoriesContainsParent(dir);
    }

    @Override
    public boolean isDirectorySelected(File dir) {
        return mFindBigFilesRequest.chosenDirectoriesContains(dir);
    }

    @Override
    public boolean onSelectDirectory(File dir) {
        return mFindBigFilesRequest.getChosenDirectories().add(dir);
    }

    @Override
    public boolean onDeselectDirectory(File dir) {
        return mFindBigFilesRequest.getChosenDirectories().remove(dir);
    }

    @Override
    public void onFindBigFilesCancelled() {
        runOnUiThread(new Runnable() {
            public void run() {
                setButtonStartFindingEnabled(true);
            }
        });
    }

    @Override
    public void onFindBigFilesFinished(final IFindBigFilesResult findBigFilesResult) {
        runOnUiThread(new Runnable() {
            public void run() {
                TabLayout.Tab tab = mTabLayout.getTabAt(TAB_IDX_FIND_RESULTS);

                if (tab != null) {
                    tab.select();

                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();

                    Intent intent = new Intent(BROADCAST_ACTION_SHOW_RESULTS);
                    intent.putExtra(BROADCAST_ARG_SHOW_RESULTS_SET_OF_RESULTS, gson.toJson(findBigFilesResult));
                    LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);

                    setButtonStartFindingEnabled(true);
                }
            }
        });
    }

    private void setButtonStartFindingEnabled(final boolean enabled) {
        mButtonStopFinding.setText(R.string.directory_chooser_button_stop_finding);
        mButtonStopFinding.setEnabled(!enabled);
        mButtonStartFinding.setEnabled(enabled);
    }

    private void initActivity() {
        Intent intent = new Intent(this, FindBigFilesService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager)findViewById(R.id.viewPager);
        mViewPager.setAdapter(mPagerAdapter);

        mTabLayout = (TabLayout)findViewById(tabLayout);
        mTabLayout.setupWithViewPager(mViewPager);

        mButtonStopFinding = (Button)findViewById(R.id.buttonStopFinding);
        mButtonStopFinding.setEnabled(false);
        mButtonStopFinding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBound && mService != null && mService.isFindBigFilesRunning()) {
                    mService.stopFindingBigFiles();
                    mButtonStopFinding.setText(R.string.directory_chooser_button_stopping);
                }
            }
        });

        mButtonStartFinding = (Button)findViewById(R.id.buttonStartFinding);
        mButtonStartFinding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mFindBigFilesRequest.getChosenDirectories().isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(R.string.dialog_text_number_of_biggest_files);

                    final EditText input = new EditText(MainActivity.this);
                    input.setInputType(InputType.TYPE_CLASS_NUMBER);
                    builder.setView(input);
                    builder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(input.getText().toString(), 10);
                            }
                            catch (Exception e) {
                                Log.v("LogBigFilesFinder", "MainActivity.mButtonStartFinding.onClick(): exception occurred during parsing big files count number: " + e.getMessage());
                            }

                            if (count > 0) {
                                if (mBound && mService != null && !mService.isFindBigFilesRunning()) {
                                    FindBigFilesRequest findBigFilesRequest = new FindBigFilesRequest(mFindBigFilesRequest);
                                    findBigFilesRequest.setBigFilesCount(count);

                                    mService.startFindingBigFiles(findBigFilesRequest);
                                    setButtonStartFindingEnabled(false);
                                }
                            }
                            else {
                                Toast.makeText(MainActivity.this, R.string.directory_chooser_text_invalid_count, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                    builder.setNegativeButton(R.string.dialog_button_cancel, null);
                    builder.show();
                }
                else {
                    Toast.makeText(MainActivity.this, R.string.dialog_text_no_directory_chosen, Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private class CustomServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            FindBigFilesService.CustomBinder binder = (FindBigFilesService.CustomBinder)service;
            mService = binder.getService();
            mService.setFindBigFilesServiceCallbacks(MainActivity.this);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }

    }

    private class CustomPagerAdapter extends FragmentPagerAdapter {

        private CustomPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.main_activity_tab_directory_chooser);

                case 1:
                    return getString(R.string.main_activity_tab_find_results);

                default:
                    return "";
            }
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new DirectoryChooserFragment();

                case 1:
                    return new FindResultsFragment();

                default:
                    return null;
            }
        }

    }

}
