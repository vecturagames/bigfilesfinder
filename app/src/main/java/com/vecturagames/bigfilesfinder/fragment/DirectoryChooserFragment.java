package com.vecturagames.bigfilesfinder.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.vecturagames.bigfilesfinder.R;
import com.vecturagames.bigfilesfinder.activity.MainActivity;
import com.vecturagames.bigfilesfinder.adapter.DirectoryChooserAdapter;
import com.vecturagames.bigfilesfinder.callback.DirectoryChooserAdapterCallbacks;
import com.vecturagames.bigfilesfinder.wrapper.RequestPermissionsWrapper;

import java.io.File;

public class DirectoryChooserFragment extends DialogFragment implements DirectoryChooserAdapterCallbacks {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mRecyclerViewLayoutManager;
    private DirectoryChooserAdapter mRecyclerViewAdapter;

    private BroadcastReceiver mBroadcastReceiver;

    private RequestPermissionsWrapper mRequestPermissionsWrapper = new RequestPermissionsWrapper();

    private DirectoryChooserAdapterCallbacks mDirectoryChooserAdapterCallbacks;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_directory_chooser, container, false);
        initFragment(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity) {
            try {
                mDirectoryChooserAdapterCallbacks = (DirectoryChooserAdapterCallbacks)context;
            }
            catch (ClassCastException e) {
                throw new ClassCastException(context.toString() + " must implement DirectoryChooserCallbacks");
            }
        }
        else {
            throw new AssertionError("DirectoryChooserFragment cannot be attached to non-Activity context.");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantedResults) {
        mRequestPermissionsWrapper.onRequestPermissionsResult(requestCode, permissions, grantedResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantedResults);
    }

    @Override
    public void onCannotOpenDirectoryListener(File dir) {
        mDirectoryChooserAdapterCallbacks.onCannotOpenDirectoryListener(dir);
        Toast.makeText(getActivity(), R.string.directory_chooser_text_cannot_open_directory, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDirectoryClickListener(File dir) {
        mDirectoryChooserAdapterCallbacks.onDirectoryClickListener(dir);
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public boolean canSelectDirectory(File dir) {
        return mDirectoryChooserAdapterCallbacks.canSelectDirectory(dir);
    }

    @Override
    public boolean isDirectorySelected(File dir) {
        return mDirectoryChooserAdapterCallbacks.isDirectorySelected(dir);
    }

    @Override
    public boolean onSelectDirectory(File dir) {
        return mDirectoryChooserAdapterCallbacks.onSelectDirectory(dir);
    }

    @Override
    public boolean onDeselectDirectory(File dir) {
        return mDirectoryChooserAdapterCallbacks.onDeselectDirectory(dir);
    }

    private void initFragment(final View rootView) {
        mRecyclerView = (RecyclerView)rootView.findViewById(android.R.id.list);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mRecyclerViewLayoutManager);

        RequestPermissionsWrapper.OnPermissionListener grantedListener = new RequestPermissionsWrapper.OnPermissionListener() {
            @Override
            public void onPermission() {
                mRecyclerViewAdapter = new DirectoryChooserAdapter(getActivity().getResources(), DirectoryChooserFragment.this);
                mRecyclerViewAdapter.createItems(Environment.getExternalStorageDirectory());
                mRecyclerViewAdapter.setEmptyView(rootView.findViewById(android.R.id.empty));
                mRecyclerView.setAdapter(mRecyclerViewAdapter);
            }
        };

        RequestPermissionsWrapper.OnPermissionListener deniedListener = new RequestPermissionsWrapper.OnPermissionListener() {
            @Override
            public void onPermission() {
                Toast.makeText(getActivity(), R.string.directory_chooser_text_cannot_list_files_permission, Toast.LENGTH_LONG).show();
            }
        };

        mRequestPermissionsWrapper.requestReadStoragePermission(this, grantedListener, deniedListener, grantedListener);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(MainActivity.BROADCAST_ACTION_OPEN_DIRECTORY)) {
                    File parentFile = (File)intent.getExtras().get(MainActivity.BROADCAST_ARG_OPEN_DIRECTORY_FILE);
                    openDirectory(parentFile);
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver, new IntentFilter(MainActivity.BROADCAST_ACTION_OPEN_DIRECTORY));
    }

    private void openDirectory(File parentFile) {
        mRecyclerViewAdapter.createItems(parentFile);
        mRecyclerView.scrollToPosition(0);
    }

}
