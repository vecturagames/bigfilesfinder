package com.vecturagames.bigfilesfinder.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.vecturagames.bigfilesfinder.BuildConfig;
import com.vecturagames.bigfilesfinder.R;
import com.vecturagames.bigfilesfinder.activity.MainActivity;
import com.vecturagames.bigfilesfinder.adapter.FindResultsAdapter;
import com.vecturagames.bigfilesfinder.callback.FindResultsAdapterCallbacks;
import com.vecturagames.bigfilesfinder.model.FindBigFilesResultList;
import com.vecturagames.bigfilesfinder.model.FindBigFilesResultSet;
import com.vecturagames.bigfilesfinder.model.IFindBigFilesResult;
import com.vecturagames.bigfilesfinder.util.Dialog;
import com.vecturagames.bigfilesfinder.util.Util;

import java.io.File;
import java.lang.reflect.Type;

public class FindResultsFragment extends DialogFragment implements FindResultsAdapterCallbacks {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mRecyclerViewLayoutManager;
    private FindResultsAdapter mRecyclerViewAdapter;

    private BroadcastReceiver mBroadcastReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_find_results, container, false);
        initFragment(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
        }
    }

    @Override
    public void onResultClickListener(File file) {
        Dialog.showOkDialog(getActivity(), file.getName(), file.getAbsolutePath());
    }

    private void initFragment(View rootView) {
        mRecyclerView = (RecyclerView)rootView.findViewById(android.R.id.list);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mRecyclerViewLayoutManager);

        mRecyclerViewAdapter = new FindResultsAdapter(getActivity().getResources(), this);
        mRecyclerViewAdapter.setEmptyView(rootView.findViewById(android.R.id.empty));
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(MainActivity.BROADCAST_ACTION_SHOW_RESULTS)) {
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();

                    IFindBigFilesResult findBigFilesResult = null;

                    if (intent.getExtras().containsKey(MainActivity.BROADCAST_ARG_SHOW_RESULTS_SET_OF_RESULTS)) {
                        String jsonString = (String)intent.getExtras().get(MainActivity.BROADCAST_ARG_SHOW_RESULTS_SET_OF_RESULTS);

                        if (jsonString != null && !jsonString.equals("")) {
                            Type type = null;
                            if (BuildConfig.FIND_BIG_FILES_RESULT_TYPE.equalsIgnoreCase("LIST")) {
                                type = new TypeToken<FindBigFilesResultList>() { }.getType();
                            }
                            else if (BuildConfig.FIND_BIG_FILES_RESULT_TYPE.equalsIgnoreCase("SET")) {
                                type = new TypeToken<FindBigFilesResultSet>() { }.getType();
                            }
                            else {
                                Util.throwAssertionFindBigFilesResultType();
                            }

                            try {
                                findBigFilesResult = gson.fromJson(jsonString, type);
                            }
                            catch (JsonSyntaxException e) {
                                Log.v("LogBigFilesFinder", "FindResultsFragment.initFragment(): exception occurred while getting IFindBigFilesResult from JSON string: " + e.getMessage());
                            }
                        }
                    }

                    if (findBigFilesResult != null) {
                        showResults(findBigFilesResult);
                    }
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver, new IntentFilter(MainActivity.BROADCAST_ACTION_SHOW_RESULTS));
    }

    private void showResults(IFindBigFilesResult findBigFilesResult) {
        mRecyclerViewAdapter.setItems(findBigFilesResult.getFiles());
        mRecyclerView.scrollToPosition(0);
    }

}
