package com.vecturagames.bigfilesfinder.wrapper;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;

public class RequestPermissionsWrapper {

    private static final int REQUEST_PERMISSIONS_READ_EXTERNAL_STORAGE = 1;

    private HashMap<Integer, OnPermissionListener> mOnPermissionGrantedListeners = new HashMap<Integer, OnPermissionListener>();
    private HashMap<Integer, OnPermissionListener> mOnPermissionDeniedListeners = new HashMap<Integer, OnPermissionListener>();

    private void requestPermissionsWrapper(Fragment fragment, String[] permissions, int requestPermissionCode, OnPermissionListener grantedListeners, OnPermissionListener deniedListeners) {
        requestPermissionsWrapper(requestPermissionCode, grantedListeners, deniedListeners);
        fragment.requestPermissions(permissions, requestPermissionCode);
    }

    private void requestPermissionsWrapper(Activity activity, String[] permissions, int requestPermissionCode, OnPermissionListener grantedListeners, OnPermissionListener deniedListeners) {
        requestPermissionsWrapper(requestPermissionCode, grantedListeners, deniedListeners);
        ActivityCompat.requestPermissions(activity, permissions, requestPermissionCode);
    }

    private void requestPermissionsWrapper(int requestPermissionCode, OnPermissionListener grantedListeners, OnPermissionListener deniedListeners) {
        if (grantedListeners != null) {
            mOnPermissionGrantedListeners.put(requestPermissionCode, grantedListeners);
        }
        if (deniedListeners != null) {
            mOnPermissionDeniedListeners.put(requestPermissionCode, deniedListeners);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantedResults) {
        boolean allPermissionsGranted = true;

        for (int grantedResult : grantedResults) {
            if (grantedResult != PackageManager.PERMISSION_GRANTED) {
                allPermissionsGranted = false;
                break;
            }
        }

        if (allPermissionsGranted) {
            if (mOnPermissionGrantedListeners.containsKey(requestCode)) {
                mOnPermissionGrantedListeners.get(requestCode).onPermission();
            }
        }
        else {
            if (mOnPermissionDeniedListeners.containsKey(requestCode)) {
                mOnPermissionDeniedListeners.get(requestCode).onPermission();
            }
        }

        mOnPermissionGrantedListeners.remove(requestCode);
        mOnPermissionDeniedListeners.remove(requestCode);
    }

    public void requestReadStoragePermission(Activity activity, OnPermissionListener grantedListener, OnPermissionListener deniedListener, OnPermissionListener alreadyHaveListener) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionsWrapper(activity, new String[] { android.Manifest.permission.READ_EXTERNAL_STORAGE }, REQUEST_PERMISSIONS_READ_EXTERNAL_STORAGE, grantedListener, deniedListener);
        }
        else {
            alreadyHaveListener.onPermission();
        }
    }

    public void requestReadStoragePermission(Fragment fragment, OnPermissionListener grantedListener, OnPermissionListener deniedListener, OnPermissionListener alreadyHaveListener) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (ContextCompat.checkSelfPermission(fragment.getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionsWrapper(fragment, new String[] { android.Manifest.permission.READ_EXTERNAL_STORAGE }, REQUEST_PERMISSIONS_READ_EXTERNAL_STORAGE, grantedListener, deniedListener);
        }
        else {
            alreadyHaveListener.onPermission();
        }
    }

    public interface OnPermissionListener {

        void onPermission();

    }
}
