package com.vecturagames.bigfilesfinder.callback;

import com.vecturagames.bigfilesfinder.model.IFindBigFilesResult;

public interface FindBigFilesServiceCallbacks {

    void onFindBigFilesCancelled();
    void onFindBigFilesFinished(IFindBigFilesResult findBigFilesResult);

}
