package com.vecturagames.bigfilesfinder.callback;

import java.io.File;

public interface FindResultsAdapterCallbacks {

    void onResultClickListener(File file);

}