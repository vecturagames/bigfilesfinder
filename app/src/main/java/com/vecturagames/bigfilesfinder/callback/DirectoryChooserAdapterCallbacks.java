package com.vecturagames.bigfilesfinder.callback;

import java.io.File;

public interface DirectoryChooserAdapterCallbacks {

    void onCannotOpenDirectoryListener(File dir);
    void onDirectoryClickListener(File dir);
    boolean canSelectDirectory(File dir);
    boolean isDirectorySelected(File dir);
    boolean onSelectDirectory(File dir);
    boolean onDeselectDirectory(File dir);

}