package com.vecturagames.bigfilesfinder.util;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import java.io.File;
import java.util.Locale;

public class Util {

    private static final float FILE_SIZE_KB_DIVIDER = 1024.0f;
    private static final String FILE_SIZE_KB_STRING = "kB";

    private static final float FILE_SIZE_MB_DIVIDER = FILE_SIZE_KB_DIVIDER * FILE_SIZE_KB_DIVIDER;
    private static final String FILE_SIZE_MB_STRING = "MB";

    public static String formatFileSize(Locale locale, long size) {
        if (size / FILE_SIZE_KB_DIVIDER > 1024.0f) {
            return String.format(locale, "%.2f", (size / FILE_SIZE_MB_DIVIDER)) + " " + FILE_SIZE_MB_STRING;
        }
        else {
            return String.format(locale, "%.2f", (size / FILE_SIZE_KB_DIVIDER)) + " " + FILE_SIZE_KB_STRING;
        }
    }

    public static Locale getLocale(Resources resources) {
        Locale locale;

        Configuration configuration = resources.getConfiguration();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = configuration.getLocales().get(0);
        }
        else {
            //noinspection deprecation
            locale = configuration.locale;
        }

        return locale;
    }

    public static int countFiles(File dir) {
        File[] files = dir.listFiles();

        int filesCount = 0;

        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    filesCount += countFiles(file) + 1;
                }
                else {
                    filesCount++;
                }
            }
        }

        return filesCount;
    }

    public static boolean canShowDir(File dir) {
        return !dir.getAbsolutePath().equalsIgnoreCase(File.separator + "sys") && !dir.getAbsolutePath().equalsIgnoreCase(File.separator + "proc");
    }

    public static String addTrailingFileSeparatorIfNotPresent(String path) {
        return !path.endsWith(File.separator) ? path + File.separator : path;
    }

    public static void throwAssertionFindBigFilesResultType() {
        throw new AssertionError("Gradle build property FIND_BIG_FILES_RESULT_TYPE should have value of \"LIST\" or \"SET\".");
    }

}
