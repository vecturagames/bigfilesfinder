package com.vecturagames.bigfilesfinder.util;

import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;

import com.vecturagames.bigfilesfinder.R;

public class Dialog {

    public static void showOkDialog(ContextThemeWrapper contextThemeWrapper, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(contextThemeWrapper);
        if (title != null && !title.equals("")) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setPositiveButton(contextThemeWrapper.getString(R.string.dialog_button_ok), null);
        builder.show();
    }

}
