package com.vecturagames.bigfilesfinder.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.vecturagames.bigfilesfinder.BuildConfig;
import com.vecturagames.bigfilesfinder.R;
import com.vecturagames.bigfilesfinder.callback.FindBigFilesServiceCallbacks;
import com.vecturagames.bigfilesfinder.model.FindBigFilesRequest;
import com.vecturagames.bigfilesfinder.model.FindBigFilesResultList;
import com.vecturagames.bigfilesfinder.model.FindBigFilesResultSet;
import com.vecturagames.bigfilesfinder.model.IFindBigFilesResult;
import com.vecturagames.bigfilesfinder.util.Util;

import java.io.File;
import java.util.HashSet;

public class FindBigFilesService extends Service {

    private static final int NOTIFICATION_ID = 1001;

    private CustomBinder mBinder = new CustomBinder();

    private FindBigFilesServiceCallbacks mFindBigFilesServiceCallbacks;

    private FindBigFilesTask mFindFilesTask;

    @Nullable
    @Override
    public CustomBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopFindingBigFiles();
        return super.onUnbind(intent);
    }

    public void setFindBigFilesServiceCallbacks(FindBigFilesServiceCallbacks findBigFilesServiceCallbacks) {
        mFindBigFilesServiceCallbacks = findBigFilesServiceCallbacks;
    }

    public void startFindingBigFiles(FindBigFilesRequest findBigFilesRequest) {
        mFindFilesTask = new FindBigFilesTask(findBigFilesRequest);
        mFindFilesTask.execute();
    }

    public void stopFindingBigFiles() {
        if (isFindBigFilesRunning()) {
            mFindFilesTask.cancel(true);
        }
    }

    public boolean isFindBigFilesRunning() {
        return mFindFilesTask != null && mFindFilesTask.getStatus() == AsyncTask.Status.RUNNING;
    }

    public class CustomBinder extends Binder {

        public FindBigFilesService getService() {
            return FindBigFilesService.this;
        }

    }

    private class FindBigFilesTask extends AsyncTask<Void, Integer, Void> {

        private NotificationManager mNotifyManager;
        private NotificationCompat.Builder mNotificationBuilder;

        private int mFilesCountInChosenDirectories;
        private int mUpdateEvery;

        private FindBigFilesRequest mFindBigFilesRequest;
        private IFindBigFilesResult mFindBigFilesResult;

        private FindBigFilesTask(FindBigFilesRequest findBigFilesRequest) {
            mFindBigFilesRequest = findBigFilesRequest;
        }

        @Override
        protected Void doInBackground(Void... param) {
            showProgressDialog();

            if (BuildConfig.FIND_BIG_FILES_RESULT_TYPE.equals("LIST")) {
                mFindBigFilesResult = new FindBigFilesResultList(mFindBigFilesRequest.getBigFilesCount());
            }
            else if (BuildConfig.FIND_BIG_FILES_RESULT_TYPE.equals("SET")) {
                mFindBigFilesResult = new FindBigFilesResultSet(mFindBigFilesRequest.getBigFilesCount());
            }
            else {
                Util.throwAssertionFindBigFilesResultType();
            }

            countFilesInChosenDirectories();

            if (isCancelled()) {
                onFindCancelled();
                return null;
            }

            mNotificationBuilder.setContentText("0/0  0%").setProgress(mFilesCountInChosenDirectories, 0, false);
            mNotifyManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());

            findBigFilesInChosenDirectories();

            if (isCancelled()) {
                onFindCancelled();
                return null;
            }

            onFindFinished(mFindBigFilesResult);

            cancelProgressDialog();
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            int progress = (int)((values[0] / (float)mFilesCountInChosenDirectories) * 100);

            mNotificationBuilder.setContentText(values[0] + "/" + mFilesCountInChosenDirectories + "  " + progress + "%").setProgress(mFilesCountInChosenDirectories, values[0], false);
            mNotifyManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
        }

        private void showProgressDialog() {
            mNotifyManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationBuilder = new NotificationCompat.Builder(FindBigFilesService.this);

            Notification notification = mNotificationBuilder
                .setContentTitle(getString(R.string.dialog_text_finding_in_progress))
                .setContentText(getString(R.string.dialog_text_preparing))
                .setSmallIcon(R.drawable.icon_notification)
                .build();

            startForeground(NOTIFICATION_ID, notification);
        }

        private void cancelProgressDialog() {
            stopForeground(true);
        }

        private void countFilesInChosenDirectories() {
            mFilesCountInChosenDirectories = 0;

            for (File dir : mFindBigFilesRequest.getChosenDirectories()) {
                if (isCancelled()) {
                    return;
                }

                mFilesCountInChosenDirectories += Util.countFiles(dir);
            }

            mUpdateEvery = mFilesCountInChosenDirectories / 100;
        }

        private void findBigFilesInChosenDirectories() {
            final HashSet<File> internalStorageChosenDirectories = new HashSet<File>();
            final HashSet<File> externalStorageChosenDirectories = new HashSet<File>();

            String externalStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();

            for (File dir : mFindBigFilesRequest.getChosenDirectories()) {
                String chosenDirPath = Util.addTrailingFileSeparatorIfNotPresent(dir.getAbsolutePath());

                if (chosenDirPath.startsWith(externalStoragePath)) {
                    externalStorageChosenDirectories.add(dir);
                }
                else {
                    internalStorageChosenDirectories.add(dir);
                }
            }

            Thread internalStorageThread = new Thread(new Runnable() {
                public void run() {
                    for (File dir : internalStorageChosenDirectories) {
                        if (isCancelled()) {
                            return;
                        }

                        findRecursive(dir);
                    }
                }
            });

            Thread externalStorageThread = new Thread(new Runnable() {
                public void run() {
                    for (File dir : externalStorageChosenDirectories) {
                        if (isCancelled()) {
                            return;
                        }

                        findRecursive(dir);
                    }
                }
            });

            internalStorageThread.start();
            externalStorageThread.start();

            try {
                internalStorageThread.join();
            }
            catch (InterruptedException e) {
                Log.v("LogBigFilesFinder", "FindBigFilesService.FindBigFilesTask.doInBackground(): interrupted exception occurred when joining internalStorageThread: " + e.getMessage());
            }

            try {
                externalStorageThread.join();
            }
            catch (InterruptedException e) {
                Log.v("LogBigFilesFinder", "FindBigFilesService.FindBigFilesTask.doInBackground(): interrupted exception occurred when joining externalStorageThread: " + e.getMessage());
            }
        }

        private void findRecursive(File dir) {
            File[] files = dir.listFiles();

            if (files != null) {
                for (File file : files) {
                    if (isCancelled()) {
                        return;
                    }

                    if (file.isFile()) {
                        mFindBigFilesResult.tryAddFile(file);

                        if (mFindBigFilesResult.getSearchedFilesCount() % mUpdateEvery == 0) {
                            onProgressUpdate(mFindBigFilesResult.getSearchedFilesCount());
                        }
                    }
                }

                for (File file : files) {
                    if (isCancelled()) {
                        return;
                    }

                    if (file.isDirectory()) {
                        findRecursive(file);
                        mFindBigFilesResult.incSearchedFilesCount();

                        if (mFindBigFilesResult.getSearchedFilesCount() % mUpdateEvery == 0) {
                            onProgressUpdate(mFindBigFilesResult.getSearchedFilesCount());
                        }
                    }
                }
            }
        }

        private void onFindCancelled() {
            cancelProgressDialog();
            mFindBigFilesServiceCallbacks.onFindBigFilesCancelled();
        }

        private void onFindFinished(final IFindBigFilesResult findBigFilesResult) {
            mFindBigFilesServiceCallbacks.onFindBigFilesFinished(findBigFilesResult);
        }

    }

}
