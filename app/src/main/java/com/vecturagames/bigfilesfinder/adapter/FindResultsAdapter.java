package com.vecturagames.bigfilesfinder.adapter;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vecturagames.bigfilesfinder.R;
import com.vecturagames.bigfilesfinder.callback.FindResultsAdapterCallbacks;
import com.vecturagames.bigfilesfinder.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class FindResultsAdapter extends RecyclerView.Adapter<FindResultsAdapter.FileBrowserViewHolder> {

    private Locale mLocale;
    private ArrayList<Item> mItems;

    private View mEmptyView;

    private FindResultsAdapterCallbacks mFindResultsAdapterCallbacks;

    public FindResultsAdapter(Resources resources, FindResultsAdapterCallbacks findResultsCallbacks) {
        mLocale = Util.getLocale(resources);
        mFindResultsAdapterCallbacks = findResultsCallbacks;
    }

    @Override
    public FindResultsAdapter.FileBrowserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_result, parent, false);
        return new FileBrowserViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(FileBrowserViewHolder viewHolder, int position) {
        viewHolder.setData(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
    }

    public void setItems(ArrayList<File> files) {
        mItems = new ArrayList<Item>();
        for (File file : files) {
            mItems.add(new Item(file));
        }
        notifyDataSetChanged();

        if (mEmptyView != null) {
            if (getItemCount() > 0) {
                mEmptyView.setVisibility(View.GONE);
            }
            else {
                mEmptyView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class Item implements Comparable<Item> {

        File mFile;

        Item(File file) {
            mFile = file;
        }

        @Override
        public int compareTo(@NonNull Item other) {
            return ((Long)other.mFile.length()).compareTo(mFile.length());
        }

    }

    class FileBrowserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Item mItem;

        ImageView mImageViewImage;
        TextView mTextViewName;
        TextView mTextViewDescription;

        FileBrowserViewHolder(View rootView) {
            super(rootView);
            mImageViewImage = (ImageView)rootView.findViewById(R.id.imageViewImage);
            mTextViewName = (TextView)rootView.findViewById(R.id.textViewName);
            mTextViewDescription = (TextView)rootView.findViewById(R.id.textViewDescription);
            rootView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mFindResultsAdapterCallbacks != null) {
                mFindResultsAdapterCallbacks.onResultClickListener(mItem.mFile);
            }
        }

        void setData(Item item) {
            mItem = item;

            mImageViewImage.setImageResource(R.drawable.icon_file);
            mTextViewName.setText(mItem.mFile.getName());
            mTextViewDescription.setText(Util.formatFileSize(mLocale, mItem.mFile.length()));
        }

    }

}
