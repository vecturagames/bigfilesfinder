package com.vecturagames.bigfilesfinder.adapter;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vecturagames.bigfilesfinder.R;
import com.vecturagames.bigfilesfinder.callback.DirectoryChooserAdapterCallbacks;
import com.vecturagames.bigfilesfinder.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class DirectoryChooserAdapter extends RecyclerView.Adapter<DirectoryChooserAdapter.FileBrowserViewHolder> {

    private Resources mResources;
    private Locale mLocale;
    private ArrayList<Item> mItems;

    private View mEmptyView;

    private DirectoryChooserAdapterCallbacks mDirectoryChooserAdapterCallbacks;

    public DirectoryChooserAdapter(Resources resources, DirectoryChooserAdapterCallbacks directoryChooserCallbacks) {
        mResources = resources;
        mLocale = Util.getLocale(resources);
        mDirectoryChooserAdapterCallbacks = directoryChooserCallbacks;

        if (mDirectoryChooserAdapterCallbacks == null) {
            throw new AssertionError("DirectoryChooserAdapter error: constructor parameter directoryChooserCallbacks cannot be null.");
        }
    }

    @Override
    public DirectoryChooserAdapter.FileBrowserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_file_browser, parent, false);
        return new FileBrowserViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(FileBrowserViewHolder viewHolder, int position) {
        viewHolder.setData(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
    }

    public boolean createItems(File parentFile) {
        boolean result = false;

        if (parentFile != null && parentFile.exists()) {
            File[] files = null;
            try {
                files = parentFile.listFiles();
            }
            catch (Exception e) {
                Log.v("LogBigFilesFinder", "DirectoryChooserAdapter.createItems(): exception occurred when listing files: " + e.getMessage());
            }

            if (files != null) {
                ArrayList<Item> dirs = new ArrayList<Item>();
                ArrayList<Item> fls = new ArrayList<Item>();

                for (File file : files) {
                    if (file.isDirectory()) {
                        if (Util.canShowDir(file)) {
                            dirs.add(new Item(file));
                        }
                    }
                    else {
                        fls.add(new Item(file));
                    }
                }

                Collections.sort(dirs);
                Collections.sort(fls);

                dirs.addAll(fls);

                if (parentFile.getParentFile() != null && parentFile.getParentFile().canRead()) {
                    dirs.add(0, new Item(parentFile.getParentFile(), true));
                }

                mItems = dirs;
                notifyDataSetChanged();

                result = true;
            }
            else {
                mDirectoryChooserAdapterCallbacks.onCannotOpenDirectoryListener(parentFile);
            }
        }

        if (mEmptyView != null) {
            if (getItemCount() > 0) {
                mEmptyView.setVisibility(View.GONE);
            }
            else {
                mEmptyView.setVisibility(View.VISIBLE);
            }
        }

        return result;
    }

    private class Item implements Comparable<Item> {

        File mFile;
        boolean mIsParentDir;

        Item(File file) {
            this(file, false);
        }

        Item(File file, boolean isParentDir) {
            mFile = file;
            mIsParentDir = isParentDir;
        }

        @Override
        public int compareTo(@NonNull Item other) {
            return mFile.getName().toLowerCase(Locale.ENGLISH).compareTo(other.mFile.getName().toLowerCase(Locale.ENGLISH));
        }

    }

    class FileBrowserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Item mItem;

        ImageView mImageViewImage;
        TextView mTextViewName;
        TextView mTextViewDescription;
        CheckBox mCheckBoxSelect;

        FileBrowserViewHolder(View rootView) {
            super(rootView);
            mImageViewImage = (ImageView)rootView.findViewById(R.id.imageViewImage);
            mTextViewName = (TextView)rootView.findViewById(R.id.textViewName);
            mTextViewDescription = (TextView)rootView.findViewById(R.id.textViewDescription);
            mCheckBoxSelect = (CheckBox)rootView.findViewById(R.id.checkBoxSelect);
            rootView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItem.mFile.isDirectory() && createItems(mItem.mFile)) {
                mDirectoryChooserAdapterCallbacks.onDirectoryClickListener(mItem.mFile);
            }
        }

        void setData(Item item) {
            mItem = item;

            if (mItem.mIsParentDir) {
                mTextViewName.setText(R.string.other_parent_directory);
                mImageViewImage.setImageResource(R.drawable.icon_directory_parent);
                mTextViewDescription.setText(R.string.directory_chooser_text_parent_directory);
                mCheckBoxSelect.setVisibility(View.GONE);
            }
            else {
                mTextViewName.setText(mItem.mFile.getName());

                if (mItem.mFile.isDirectory()) {
                    mImageViewImage.setImageResource(R.drawable.icon_directory);

                    File[] files = mItem.mFile.listFiles();

                    String text;
                    int filesCount = files != null ? files.length : 0;
                    if (filesCount == 0) {
                        text = mResources.getString(R.string.directory_chooser_text_files_nothing);
                    }
                    else if (filesCount == 1) {
                        text = mResources.getString(R.string.directory_chooser_text_files_singular);
                    }
                    else if (filesCount > 1 && filesCount < 5) {
                        text = String.format(mResources.getString(R.string.directory_chooser_text_files_plural_2_3_4), filesCount);
                    }
                    else {
                        text = String.format(mResources.getString(R.string.directory_chooser_text_files_plural_5_and_more), filesCount);
                    }

                    mTextViewDescription.setText(text);

                    mCheckBoxSelect.setVisibility(View.VISIBLE);
                    mCheckBoxSelect.setEnabled(files != null && !mDirectoryChooserAdapterCallbacks.canSelectDirectory(mItem.mFile));
                    mCheckBoxSelect.setOnCheckedChangeListener(null);
                    mCheckBoxSelect.setChecked(mDirectoryChooserAdapterCallbacks.isDirectorySelected(mItem.mFile));
                    mCheckBoxSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                mDirectoryChooserAdapterCallbacks.onSelectDirectory(mItem.mFile);
                            }
                            else {
                                mDirectoryChooserAdapterCallbacks.onDeselectDirectory(mItem.mFile);
                            }
                        }
                    });
                }
                else {
                    mImageViewImage.setImageResource(R.drawable.icon_file);
                    mTextViewDescription.setText(Util.formatFileSize(mLocale, mItem.mFile.length()));
                    mCheckBoxSelect.setVisibility(View.GONE);
                }
            }
        }

    }

}
